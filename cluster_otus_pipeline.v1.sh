# modeled after http://drive5.com/usearch/manual/uparse_cmds.html
# few syntax corrections

# input: fastq input file
fq=$1

# Set a variable to give a short name for the USEARCH binary
u=/vol100/bin/usearch
derep=0 # dereplicate or not ? 

# Variable for directory containing input data (reads and ref. db)
fq=`basename $fq`
d=`dirname $fq`
cd $d


if [ ! -s fa=${fq%\.fastq}_maxee-5_trunc-200.fa ]; then
# Quality filter, length truncate, covert to FASTA
    $u -fastq_filter $fq -fastq_maxee 5 -fastq_trunclen 200 -fastaout ${fq%\.fastq}_maxee-5_trunc-200.fa
fi

fa=${fq%\.fastq}_maxee-5_trunc-200.fa

# extra global_trimming step ?? 
# cluster_otus command considers all gaps to be differences, including terminal gaps
# $u -fastx_truncate $fa -trunclen 200 -label_suffix _200 -fastaout ${fa%\.fa}_200.fa

if(($derep)); then  
# dereplication step allows abundance sort,
# which makes for better centroids (and hence better clustering?)
# but obfuscates DB/MB counts.. would have to write code to trace them all back 
    $u -derep_fulllength $fa -fastaout ${fa%\.fa}_derep-full.fa -sizeout -uc ${fa%\.fa}_derep-full.uc
    fa=${fa%\.fa}_derep-full.fa

# Abundance sort but don't discard singletons # 
# relies on dereplication step?
# "Use -fastaout and/or -fastqout, not -output"
    $u -sortbysize $fa -fastaout ${fa%\.fa}_sort-abun.fa -minsize 1
    fa=${fa%\.fa}_sort-abun.fa
fi

# OTU clustering
$u -cluster_otus $fa -otus ${fa%\.fa}_centroids.fa   -sizein  -sizeout -uparseout ${fa%\.fa}.upout

