#!/usr/bin/perl -w
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# Given the outfiles from a few blast runs
# consolidates so each query's results is found on contiguous lines,
# ordered by the bit Score
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
use strict;
use Getopt::Long;
my $debug=0;
my $hash_field_num = 0; # hash on query by default like standard blast results
my $min_bit_score = 30;

GetOptions ( 'debug!' => \$debug , 
             'min_bit_score:i'  => \$min_bit_score ,
             'hash_field_num:i'  => \$hash_field_num , 
	     );

my $outfile = pop(@ARGV);
my @infiles = @ARGV or die "supply blast infiles to hash, outfile";
if ( -s $outfile ) { die "$outfile already exists"; }

print STDERR "filtering out results with bit score less than $min_bit_score\n" .
    "ordering by field num $hash_field_num and then bit score\n";

my %hash = (); 
foreach my $infile (@infiles) {
    open my $INFILE, "< $infile " or die "can't open $infile";
    my $count_records_to_keep = 0 ;
    while(<$INFILE>){
	chomp;
	my @F = split;
	if( 1 or $F[11]>$min_bit_score ){ 
	    push @{$hash{$F[$hash_field_num]}}, $_ ;
	    $count_records_to_keep++ ;
	};
	last if $. >=1000 && $debug;
    }
    close $INFILE; 
    print STDERR "$infile \t $count_records_to_keep \n";
}



open my $OUTFILE, "> $outfile " or die "can't open  $outfile" ;
foreach my $query (keys %hash ) {
    my @lines_ordered = sort { (split /\t/,$b)[11] <=> (split /\t/,$a)[11] } @{$hash{$query}};
    print $OUTFILE join("\n", @lines_ordered )."\n" ;
}
close $OUTFILE;




