#!/usr/bin/python
import pdb

import sys
import Levenshtein as Lev
from itertools import combinations
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#

maxdist = int(sys.argv[1])
infile = sys.argv[2]
queries = sys.argv[3:]
#out_hist = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in in_ ] )
#fh = open ( out_hist + '.hamming_dist.hist', 'w')


fh = open(infile, "r")
for line in fh: 
    line=line.strip()
    fields = line.split()
    for q in queries:
        if len(fields[0]) == len(q):
            d = Lev.hamming(fields[0], q) 
            if d <= maxdist :
                print q+"\t" + str(d) + "\t"+ line 
fh.close()


