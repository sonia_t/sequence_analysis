
This directory should contain 11 following files in CSV format.
1_Summary_test_fasta_30052014.txt:	the synthesis of the analysis
2_IMGT-gapped-nt-sequences_test_fasta_30052014.txt:	includes the nucleotide (nt) sequences of labels that have been gapped according to the IMGT unique numbering
3_Nt-sequences_test_fasta_30052014.txt:	includes the ungapped nt sequences of all described labels
4_IMGT-gapped-AA-sequences_test_fasta_30052014.txt:	includes the AA sequences of labels that have been gapped according to the IMGT unique numbering
5_AA-sequences_test_fasta_30052014.txt:	includes the ungapped AA sequences of labels
6_Junction_test_fasta_30052014.txt:	includes the results of IMGT/JunctionAnalysis
7_V-REGION-mutation-and-AA-change-table_test_fasta_30052014.txt:	includes the list of mutations (nt mutations, AA changes, AA class identity (+) or change (-), ...
8_V-REGION-nt-mutation-statistics_test_fasta_30052014.txt:	includes the number (nb) of nt positions including IMGT gaps, the nb of nt, the nb of identical nt, ...
9_V-REGION-AA-change-statistics_test_fasta_30052014.txt:	includes the nb of AA positions including IMGT gaps, the nb of AA, the nb of identical AA, ...
10_V-REGION-mutation-hotspots_test_fasta_30052014.txt:	indicates the localization of the hot spots motifs detected in the closest germline V-REGION with positions in FR-IMGT and CDR-IMGT
11_Parameters_test_fasta_30052014.txt:	includes the date of the analysis, the IMGT/V-QUEST version, and the parameters used for the analysis
For more information on these files, please visit the IMGT/HighV-QUEST Documentation page.


Acknowledgements: this work was granted access to the HPC resources of CINES under the allocation 2013-036029 made by GENCI (Grand Equipement National de Calcul Intensif).
Please quote: Alamyar E., Giudicelli V., Li S., Duroux P., Lefranc M.-P. Immunome Research, 2012.