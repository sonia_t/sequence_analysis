#!/usr/bin/python
import os
import pdb
import sys
from Bio import SeqIO
import Levenshtein as Lev
from  itertools import combinations



if len(sys.argv) < 1:
    sys.stderr.write(" python pairs_Lev_sim.py in.fasta in2.fasta\n")
    sys.stderr.write(" output is a matrix with rows (file1) and cols(file2) \n")

in_fasta = sys.argv[1:3]
out_mat = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in in_fasta ] )
fh = open ( out_mat + '.Lev_ratio.mat', 'w')


recs1 = SeqIO.parse(  open(in_fasta[0], "r"), "fasta") # generator
recs2_d =  SeqIO.to_dict(  SeqIO.parse(  open(in_fasta[1], "r"),'fasta') )

fh.write( "ids\t" +"\t".join( [ rec2.id for rec2 in recs2_d.values() ]) +"\n") #colnames

for rec1 in recs1:  # each row
    fh.write( rec1.id )
    for rec2 in recs2_d.values() :
        r = Lev.ratio( str( rec1.seq), str( rec2.seq))
#        sys.stdout.write( " ".join( [ str( rec1.seq), str( rec2.seq), " r= "+ r ,"\n"] ))
        fh.write( "\t%.5g" % r)
    fh.write( "\n")
fh.close()
