#!/usr/bin/python
import os
import pdb
import sys
from Bio import SeqIO
import Levenshtein as Lev
from  itertools import combinations



if len(sys.argv) < 1:
    sys.stderr.write(" python pairs_Lev_sim.py in.fasta\n")
    sys.exit

in_fasta_F = sys.argv[1]
path = os.path.split(in_fasta_F)[0] # doesn't seem to get anything 

#seqs = SeqIO.to_dict(SeqIO.parse(  open(in_fasta_F, "r"), "fasta"))

lev_ratios={}

recs=SeqIO.parse(  open(in_fasta_F, "r"), "fasta") # generator

f= open ( path + in_fasta_F + '.Lev_distpairs', 'w')

# output: pairs 
dist_dict = {}
for (rec1,rec2) in combinations(recs,2):
    r = Lev.ratio( str( rec1.seq), str( rec2.seq))
    f.write( ' '.join(sorted([rec1.id,rec2.id])) + "\t%.5g\n" % r)
f.close()
