#!/usr/bin/python
import os
import pdb
import sys
from Bio import SeqIO
import Levenshtein as Lev
from  itertools import combinations
from optparse import OptionParser

parser=OptionParser()

parser.add_option("-i", "--input_format",
                  dest="input_format", choices=('fasta','list'),
                  help="mandatory to specify an input format [fasta/list]", metavar="infiles")
parser.add_option("-o", "--outfile", dest="outfile",
                  help="write output to outfile", metavar="outfile")
parser.add_option("-d", "--dist_func",
                  dest="dist_func", choices=('Lev.ratio','hamming','Lev.distance'),
                  help="mandatory to specify a distance function")

(options, args) = parser.parse_args()
dist_func = { 'Lev_ratio' : Lev.ratio , 'Lev_distance': Lev.distance, 'hamming' : Lev.hamming }[options.dist_func]
if len(args) == 0:
    sys.stderr.write(" Python pairs_Lev_sim.py in1 in2\n")
    sys.stderr.write(" Output is a matrix with rows (file1) and cols(file2) \n")
    sys.exit(2)
elif len(args) == 1 :
    args.append(args[0])

infiles = args
if infiles[0] == infiles[1]:
    out_mat = '.vs.'.join( [ os.path.split(infiles[0])[-1], 'self' ] )
else:
    out_mat = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in infiles ] )
fh = open ( '.'.join( [out_mat, options.dist_func, 'mat'] ), 'w')


if options.input_format == "fasta":  
    in_fasta=infiles
    recs1 = SeqIO.parse(  open(in_fasta[0], "r"), "fasta") # generator
    recs2_d =  SeqIO.to_dict(  SeqIO.parse(  open(in_fasta[1], "r"),'fasta') ) # wish it retained order , especially in case where both files are the same. 

    fh.write( "ids\t" +"\t".join( [ rec2.id for rec2 in recs2_d.values() ]) +"\n") #colnames

    for rec1 in recs1:  # each row
        fh.write( rec1.id )
        for rec2 in recs2_d.values() :  
            r = dist_func( str( rec1.seq), str( rec2.seq))
    #        if r==1: sys.stdout.write( " ".join( [ str( rec1.seq), str( rec2.seq), " r= %.5g\n" %  r] ))
            fh.write( "\t%.5g" % r)
        fh.write( "\n")
elif options.input_format == "list":  
    with open(infiles[1]) as f:
        strings2 = [ x.rstrip().split(None, 1)[0] for x in f if x[0]!="#"]  # list 
    fh.write( "strings\t" +"\t".join( [ string2 for string2 in strings2 ]) +"\n") #colnames are in file 2
    f1 = open(infiles[0],'r')
    for line in (x for x in f1 if not x.startswith('#')):
        string1 = x.rstrip().split(None, 1)[0]
        fh.write( string1 ) 
        for string2 in strings2:
            r = dist_func( string1, string2 )
            fh.write( "\t%.5g" % r)
        fh.write( "\n")

fh.close()
