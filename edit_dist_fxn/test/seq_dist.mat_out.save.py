#!/usr/bin/python
import os
import pdb
import sys
from Bio import SeqIO
import Levenshtein as Lev
from  itertools import combinations
from optparse import OptionParser

parser=OptionParser()

parser.add_option("-f", "--file", dest="filename",
                  help="write report to FILE", metavar="FILE")
parser.add_option("-d", "--dist_func",
                  dest="dist_func", choices=('Lev_ratio','hamming'),
                  help="mandatory to specify a distance function")

(options, args) = parser.parse_args()
dist_func = { 'Lev_ratio' : Lev.ratio , 'hamming' : Lev.hamming }[options.dist_func]

if len(args) < 1:
    sys.stderr.write(" python pairs_Lev_sim.py in.fasta in2.fasta\n")
    sys.stderr.write(" output is a matrix with rows (file1) and cols(file2) \n")

in_fasta = args
out_mat = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in in_fasta ] )
fh = open ( '.'.join( [out_mat, options.dist_func, 'mat'] ), 'w')

recs1 = SeqIO.parse(  open(in_fasta[0], "r"), "fasta") # generator
recs2_d =  SeqIO.to_dict(  SeqIO.parse(  open(in_fasta[1], "r"),'fasta') )

fh.write( "ids\t" +"\t".join( [ rec2.id for rec2 in recs2_d.values() ]) +"\n") #colnames

for rec1 in recs1:  # each row
    fh.write( rec1.id )
    for rec2 in recs2_d.values() :
        r = dist_func( str( rec1.seq), str( rec2.seq))
#        if r==1: sys.stdout.write( " ".join( [ str( rec1.seq), str( rec2.seq), " r= %.5g\n" %  r] ))
        fh.write( "\t%.5g" % r)
    fh.write( "\n")
fh.close()
