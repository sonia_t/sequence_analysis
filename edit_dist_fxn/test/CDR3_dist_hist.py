#!/usr/bin/python
import os
import pdb
import sys
from Bio import SeqIO
import Levenshtein as Lev
from  itertools import combinations
from optparse import OptionParser

parser=OptionParser()

parser.add_option("-f", "--file", dest="filename",
                  help="write report to FILE", metavar="FILE")

(options, args) = parser.parse_args()


if len(args) < 1:
    sys.stderr.write(" CDR3_dist_hist.py CDR3_counts\n")
    sys.stderr.write(" output is a histogram \n")

infile = args
fh = open ( '.'.join( [infile[0], 'dist_hist'] ), 'w')


with open(infile[0]) as f:
  string2count = dict(x.rstrip().split()[0:2] for x in f) # only consider the first column. 

#print( "\n".join([  k +"\t" +string2count[k]  for k in string2count]))

#recs1 = SeqIO.parse(  open(in_fasta[0], "r"), "fasta") # generator
#recs2_d =  SeqIO.to_dict(  SeqIO.parse(  open(in_fasta[1], "r"),'fasta') )
#fh.write( "ids\t" +"\t".join( [ rec2.id for rec2 in recs2_d.values() ]) +"\n") #colnames


dist_hist = {k:0 for k in range(300)}

for string1 in string2count.keys():  
    dist_hist[0] += int(string2count[string1]) #    dist_hist[string1][0] = string2count[string1]

for (string1,string2) in combinations(string2count.keys(),2):
       if len(string1) == len(string2): 
            dist = Lev.hamming( string1, string2) 
            dist_hist[dist] += int(string2count[string2])* int(string2count[string1])  #           dist_hist[string1][dist] += string2count[string2]


fh.write( "\n".join([ "%d\t%d" % (k,dist_hist[k])  for k in sorted(dist_hist)] ))   #  "\n".join([ str(k) + "\t" + str(d[k]) for k in sorted(d)] ))


fh.write( "\n")
fh.close()
