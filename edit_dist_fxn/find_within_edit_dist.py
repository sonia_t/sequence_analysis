#!/usr/bin/python
import pdb

import sys
import Levenshtein as Lev
from itertools import combinations
from optparse import OptionParser

# # # DEFAULTS
out_fh=sys.stderr

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

parser = OptionParser()
parser.add_option("-o", "--outfile", dest="outfile",
                  help="write output to outfile", metavar="outfile")
parser.add_option("-q", "--queries_file", dest="queries_file",
                  help="get queries from file instead of command line")
parser.add_option("-c", "--subject_col", dest="subject_col", default=0,
                  help="column in the subject file")
parser.add_option("-d", "--dist_func",
                  dest="dist_func", choices=('Lev.ratio','hamming','Lev.distance'),
                  help="mandatory to specify a distance function")
parser.add_option("--max_dist",
                  dest="max_dist", 
                  default = 1,
                  help="maximum edit dist")
parser.add_option("--min_dist",
                  dest="min_dist", 
                  default = 0,
                  help="minimum edit dist")
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#

(options, args) = parser.parse_args()
dist_func = { 'Lev.ratio' : Lev.ratio , 'Lev.distance': Lev.distance, 'hamming' : Lev.hamming }[options.dist_func]
max_dist = float(options.max_dist)
if options.outfile: out_fh = open(options.outfile, "w")



subj_file = args[0]
if options.queries_file:
    with open(options.queries_file) as f:
        queries = [ x.rstrip().split(None, 1)[0] for x in f if x[0]!="#"]  # list  
else:
    queries = args[1:]
#out_hist = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in in_ ] )
#fh = open ( out_hist + '.hamming_dist.hist', 'w')

(count, infile_lines)=(0,0)
in_fh = open(subj_file, "r")

for line in in_fh: 
    line = line.strip()
    fields = line.split()
    for q in queries:
#        if len(fields[0]) == len(q):
        d = dist_func(fields[int(options.subject_col)], q) 
        if d >= float(options.min_dist) and d <= float(options.max_dist) :
            count += 1
            out_fh.write(q+"\t" + str(d) + "\t"+ line+"\n")
    infile_lines += 1
in_fh.close()

results_summary = "%d queries, %d subjects. %d matches  within %s %d, more than minimum dist %s  \n" % (len(queries), infile_lines ,count, options.dist_func, max_dist, options.min_dist) 
sys.stdout.write(results_summary + "CALL:" +  ' '.join(sys.argv)  + "\n" )

