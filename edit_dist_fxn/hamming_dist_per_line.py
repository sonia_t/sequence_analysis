#!/usr/bin/python
import pdb

import sys
import Levenshtein as Lev
from itertools import combinations
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#

infile = sys.argv[1]
#out_hist = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in in_ ] )
#fh = open ( out_hist + '.hamming_dist.hist', 'w')


dist = dict( [(d,0) for d in range(16)] )
fh = open(infile, "r")
for line in fh: 
    fields = line.split()
    dists_this_line = []
    for (MB1,MB2) in combinations(fields[1:],2):
        if len(MB1) == len(MB2):
            d=Lev.hamming(MB1,MB2)
            dist[d]+=1
fh.close()

print "\n".join( [ "%s\t%s" % (k,v) for k,v in dist.iteritems()] )
