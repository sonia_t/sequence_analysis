#!/usr/bin/python
import pdb

import sys
import Levenshtein as Lev
from itertools import combinations
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#

infile = sys.argv[1]
#out_hist = '.vs.'.join( [ os.path.split(fp)[-1]  for fp in in_ ] )
#fh = open ( out_hist + '.hamming_dist.hist', 'w')


fh = open(infile, "r")
for line in fh: 
    fields = line.split()
    dists_this_line = []
    for (MB1,MB2) in combinations(fields[1:],2):
        dists_this_line.append( Lev.ratio(MB1,MB2) )
    sys.stdout.write( fields[0] + " %.3f" *len(dists_this_line) % tuple(dists_this_line) +"\n")
fh.close()

# print "\n".join( [ "%s\t%s" % (k,v) for k,v in dist.iteritems()] )
